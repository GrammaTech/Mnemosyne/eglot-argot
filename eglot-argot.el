;;; eglot-argot.el --- Support Argot LSP extensions in Eglot

;; Copyright (C) 2021 GrammaTech, Inc.

;; Author: Paul M. Rodriguez <prodriguez@grammatech.com>
;; Version: 0.2.3
;; Package-Requires: ((eglot "1.7"))
;; Keywords: convenience, languages
;; URL: https://gitlab.com/GrammaTech/Mnemosyne/eglot-argot

;; This file is not part of GNU Emacs.

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.



;;; Commentary:

;; Extends Eglot to support Argot LSP extensions.

(require 'cl-lib)
(require 'eglot)
(require 'vc)
(require 'project)

(defgroup eglot-argot nil
  "Use Eglot with Argot LSP"
  :prefix "eglot-argot-"
  :group 'eglot)

(defcustom eglot-argot-server-host "localhost"
  "Host name to connect to."
  :group 'eglot-argot
  :type 'string)

(defcustom eglot-argot-server-port 10003
  "Port to connect to."
  :group 'eglot-argot
  :type 'number)

(defcustom eglot-argot-disable nil
  "List of names of muses to disable."
  :group 'eglot-argot
  :tag "Disabled muses"
  :type '(repeat string))

(defun eglot-argot-host+port ()
  (list eglot-argot-server-host
        eglot-argot-server-port))

;;;###autoload
(defun mnemosyne (&optional arg)
  "Connect the current buffer to Mnemosyne."
  (interactive "P")
  (let ((eglot-server-programs
         (if arg
             eglot-server-programs
             (list (cons '(python-mode
                           js-mode typescript-mode
                           c++-mode c-mode
                           lisp-mode)
                         (eglot-argot-host+port))))))
    (cl-destructuring-bind (mode project _class contact language-id)
        (eglot--guess-contact arg)
      (eglot mode
             project
             'argot-server
             (if arg contact (eglot-argot-host+port))
             language-id))))

(defalias 'argot 'mnemosyne)

(defun eglot-argot-server-programs ()
  (let ((host-port (eglot-argot-host+port)))
    `((lisp-mode . (argot-server ,host-port))
      (js-mode . (argot-server ,host-port))
      (python-mode . (argot-server ,host-port))
      (c-mode . (argot-server ,host-port))
      (c++-mode . (argot-server ,host-port)))))

(defun remove-argot-programs (programs)
  (let ((argot-server-programs (eglot-argot-server-programs)))
    (cl-remove-if (lambda (x)
                    (member x argot-server-programs))
                  eglot-server-programs)))

;;;###autoload
(define-minor-mode eglot-argot-mode
  "Configure Eglot to use (or not to use) Argot LSP."
  (setf eglot-server-programs
        (append (eglot-argot-server-programs)
                eglot-server-programs))
  :init-value nil
  :after-hook
  (setf eglot-server-programs
        (if eglot-argot-mode
            (append (eglot-argot-server-programs)
                    (remove-argot-programs eglot-server-programs))
          (remove-argot-programs eglot-server-programs))))



(defclass argot-server (eglot-lsp-server)
  ()
  :documentation "Argot LSP server.")

(cl-defmethod eglot-handle-request
  ((server argot-server)
   (_method (eql argot/window/showPrompt))
   &key type message actions defaultValue multiLine)
  "Handle server request argot/window/showPrompt"
  (let* ((actions (append actions nil)) ;; gh#627
         (label (completing-read
                 (concat
                  (format (propertize "[eglot] Server reports (type=%s): %s"
                                      'face (if (<= type 1) 'error))
                          type message)
                  "\nChoose an option: ")
                 (or (mapcar (lambda (obj) (plist-get obj :title)) actions)
                     '("OK"))
                 nil nil (plist-get (elt actions 0) :title))))
    (if label `(:title ,label) :null)))

(cl-defmethod eglot-handle-request
  ((server argot-server)
   (_method (eql argot/workspace/files))
   &key base)
  "Handle SERVER request argot/workspace/files"
  (if base
      (setf base
            (eglot--uri-to-path base))
      (let* ((project (project-current)))
        (setf base (project-root project))))
  (let* ((project (project-current nil base))
         (files (project-files project))
         (eglot--uri-path-allowed-chars
          ;; This is a bug in Eglot, but I'm not spending an hour
          ;; filling out a template to report it.
          (let ((vec (copy-sequence eglot--uri-path-allowed-chars)))
            (aset vec ?% nil)
            vec)))
    (cl-map 'vector
            (lambda (file)
              `(:uri ,(eglot--path-to-uri file)))
            (project-files project))))

(cl-defmethod eglot-handle-request
  ((_server argot-server)
   (_method (eql argot/textDocument/content))
   &key textDocument)
  "Handle SERVER request argot/textDocument/content"
  (let* ((uri (cl-getf textDocument :uri))
         (file (eglot--uri-to-path uri)))
    (if-let (b (find-buffer-visiting file))
        (with-current-buffer b
          (eglot--TextDocumentItem))
      ;; TODO Set the major mode to get the right language key?
      (with-temp-buffer
        (insert-file-contents file)
        (when (search-forward "\C-@")
          (error "Binary file!"))
        (let ((text
               (eglot--widening
                (buffer-substring-no-properties (point-min) (point-max)))))
          `(:uri ,uri :version 0 :languageId "" :text ,text))))))

(cl-defmethod eglot-initialization-options ((_server argot-server))
  (cl-labels ((dict* (table &rest kvs)
                     (cl-loop for (k v . nil) on kvs by #'cddr
                              do (setf (gethash k table) v))
                     table)
              (dict (&rest kvs)
                    (apply #'dict* (make-hash-table :test 'equal) kvs)))
    (dict* (cl-call-next-method)
           "argot" (dict "disable" eglot-argot-disable
                         "filesProvider" t
                         "contentProvider" t
                         "inputBoxProvider" t))))

(provide 'eglot-argot)
